package com.nordea.javabasics.tasks.atm;

import com.google.common.collect.Multiset;
import org.junit.Ignore;
import org.junit.Test;

import static com.nordea.javabasics.tasks.atm.Bill.*;
import static org.assertj.guava.api.Assertions.assertThat;

public class ATMTest {
	private static final String NIK = "nik";
	private static final String PIN = "pin";
	
	private ATM atm;
	
	@Test
	@Ignore("Remove after finishing the task")
	public void withdrawMoney() throws InvalidNikOrPinException {
		// given
		final long amountToWithdraw = 1234;
		
		// when
		Multiset<Bill> withdrawnMoney = atm.withdrawMoney(NIK, PIN, amountToWithdraw);
		
		// then
		assertThat(withdrawnMoney)
			.containsOnly($100, $20, $10, $2)
			.contains(12, $100)
			.contains(1, $20)
			.contains(1, $10)
			.contains(2, $2);
	}
}