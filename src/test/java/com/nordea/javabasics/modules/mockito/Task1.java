package com.nordea.javabasics.modules.mockito;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

/**
 * Stwórz za pomocą Mockito spy'a i sprawdź czy została podjęta próba dodania do zbioru tego samego elementu 2 razy.
 */
@RunWith(MockitoJUnitRunner.class)
public class Task1 {
	private final Set<Integer> set = new HashSet<>();
	
	@Test
	@Ignore("Remove after finishing the task")
	public void shouldTryToAddTheSameElementTwiceToTheSet() {
		// given
		final int element = 1234;
		
		// when
		set.add(element);
		set.add(element);
		
		// then
		// TODO
	}
}
