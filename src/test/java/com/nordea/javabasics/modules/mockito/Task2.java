package com.nordea.javabasics.modules.mockito;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Stwórz mock'a iterowalnego obiektu, który będzie zwracał iterator z predefiniowanym zestawem danych.
 */
@RunWith(MockitoJUnitRunner.class)
public class Task2 {
	private Iterable<Integer> iterable;
	
	@Test
	@Ignore("Remove after finishing the task")
	public void shouldReturnSequenceOfPredefinedNumbers() {
		// given
		final Integer[] elements = { 1, 2, 3, 4 };
		
		// and
		// TODO
		
		// expect
		assertThat(iterable).containsExactly(elements);
		assertThat(iterable).containsExactly(elements); // powinno się go dać wywołać dowolną liczbę razy
	}
}
