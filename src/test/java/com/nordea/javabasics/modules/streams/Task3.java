package com.nordea.javabasics.modules.streams;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Używając wyłącznie klas IntStream, Stream oraz Collectors stwórz generator liczb od 1 do 100.
 *
 * Następnie wybierz 10 pierwszych liczb podzielnych przez 5 i pogrupuj je na liczby parzyste i nieparzyste.
 */
public class Task3 {
	@Test
	@Ignore("Remove after finishing the task")
	public void shouldFindFirst10NumbersDividableBy5AndGroupThemByTheirParity() {
		// given
		final int lowerBound = 1;
		final int upperBound = 100;
		
		// when
//		Map<Boolean, List<Integer>> result = <twoje rozwiązanie>
		
		// then
//		assertThat(result).containsEntry(false, Arrays.asList(5, 15, 25, 35, 45)); TODO odkomentuj po zmianie
//		assertThat(result).containsEntry(true, Arrays.asList(10, 20, 30, 40, 50)); TODO odkomentuj po zmianie
	}
}
