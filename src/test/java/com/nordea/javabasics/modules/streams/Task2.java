package com.nordea.javabasics.modules.streams;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Używając wyłącznie klasy IntStream wybierz same liczby niepatrzyste, podnieś do drugiej potęgi i zwróc tablicę z wynikiem.
 */
public class Task2 {
	@Test
	@Ignore("Remove after finishing the task")
	public void shouldFilterOddNumbersAndRaiseToThePowerOfTwo() {
		// given
		final int[] numbers = { 1, 2, 3, 4, 5 };
		
		// when
//		int[] result = <twoje rozwiązanie>
		
		// then
//		assertThat(result).containsExactly(1, 9, 25); TODO odkomentuj po zmianie
	}
}
