package com.nordea.javabasics.modules.streams;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Używając wyłącznie klas Stream oraz IntStream zsumuj wszystkie liczby z dwuwymiarowej macierzy.
 */
public class Task1 {
	@Test
	@Ignore("Remove after finishing the task")
	public void shouldSumAllIntegersOf2DMatrix() {
		// given
		final int[][] intMatrix = {
			{ 1, 2, 3, 4 },
			{ 1, 2, 3, 4 },
			{ 1, 2, 3, 4 }
		};
		
		// when
//		int sum = <twoje rozwiązanie>
		
		// then
//		assertThat(sum).isEqualTo(30); TODO odkomentuj po zmianie
	}
}
