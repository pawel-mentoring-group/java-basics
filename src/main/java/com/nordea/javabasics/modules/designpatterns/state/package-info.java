/*
 * Wykorzystując wzorzec stanu zamodeluj dokument wniosku oraz jego przejścia stanów.
 *
 * Dostępne dane:
 * - treść wniosku
 * - autor wniosku
 * - osoba zatwierdzająca
 *
 * Możliwe są następujące przejścia stanów:
 * - nowy -> wypełniony
 * - wypełniony -> wypełniony (poprawienie danych)
 * - wypełniony -> zatwierdzony
 *
 * UWAGA: Zabraniam używania publicznych setterów do zmiany stanu lub danych wniosku.
 *
 * Do zadania należy również utworzyć odpowiednie testy jednostkowe w tym samym pakiecie.
 */
package com.nordea.javabasics.modules.designpatterns.state;