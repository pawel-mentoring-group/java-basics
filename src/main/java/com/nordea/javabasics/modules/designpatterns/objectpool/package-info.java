/*
 * Wykorzystując wzorzec puli obiektów zaimplementuj wypożyczalnię samochodów.
 *
 * Wymagania:
 * - na starcie wypożyczalnia otrzymuje kilka pojazdów do wynajęcia (kolekcja przekazana przez konstruktor)
 * - klienci mogą wypożyczać samochody i je potem zwracać jak już przestaną ich używać
 * - klienci dla których nie ma dostępnych samochodów czekają określoną ilość czasu i po tym rezygnują
 * - wypożyczalnia nie przyjmuje samochodów innych niż te które sama wypożyczyła
 * - dla wypożyczalni należy napisać odpowiednie testy jednostkowe.
 *
 * UWAGI:
 * - proponuję zapoznać się najpierw z istniejącymi implementacjami kolejek, map i zbiorów
 * - sam samochód może być reprezentowany przez jakąś anemiczną klasę Car
 */
package com.nordea.javabasics.modules.designpatterns.objectpool;