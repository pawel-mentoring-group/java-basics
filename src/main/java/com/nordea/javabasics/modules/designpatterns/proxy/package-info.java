/*
 * Zadanie polega na napisaniu proxy logującego wywołania metod dowolnego obiektu.
 *
 * Logować należy nazwę metody, argumenty wywołania i wynik:
 * - nic (dla metod zwracających void)
 * - wartość
 * - lub rzucony wyjątek
 *
 * Najpierw trzeba stworzyć adnotację, którą będzie można osadzać na klasach lub metodach, np.: @LogInvocations.
 * Będzie ona oznaczać, które metody lub wszystkie metody danej klasy należy logować.
 * Adnotacje powinno się kłaść tylko na metodach i klasy konkretnej implementacji.
 *
 * Następnie należy wykorzystać wbudowaną fabrykę java.lang.reflect.Proxy do utworzenia samego proxy.
 * Proxy w sposób przeźroczysty powinien opakowywać zachowanie oryginalnego obiektu.
 *
 * Do zapisu logów dla uproszczenia proponuję użyć klasy java.util.logging.Logger lub zwykłego System.out.println().
 * Przykładowe formaty logów:
 * - serviceMethod1(): wartość
 * - serviceMethod2()
 * - serviceMethod3() thrown: java.lang.Exception: exception message
 * (zwykły toString() dla wartości oraz wyjątków)
 *
 * Na potrzeby zadania wymagane będzie stworzenie przykładowego interfejsu i jego implementację.
 */
package com.nordea.javabasics.modules.designpatterns.proxy;