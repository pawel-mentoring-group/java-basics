package com.nordea.javabasics.modules.generics;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;

public class Task1 {
	public static void main(String[] args) {
		ImmutableCollection<Number> numbers = ImmutableSet.of(1, 2L, 3f, 4d);
		ImmutableCollection<Integer> integers = ImmutableList.of(1, 2, 3, 4);
		ImmutableCollection<Double> doubles = ImmutableList.of(1., 2., 3., 4.);
		
		System.out.println(sumNumbers(numbers));
//		System.out.println(sumNumbers(integers)); TODO odkomentuj po zmianie
//		System.out.println(sumNumbers(doubles)); TODO odkomentuj po zmianie
	}
	
	//TODO Zmodyfikuj sygnaturę metody tak, żeby akceptowała dowolną kolekcję z liczbami.
	private static double sumNumbers(Collection<Number> numbers) {
		return numbers.stream().mapToDouble(Number::doubleValue).sum();
	}
}
