package com.nordea.javabasics.modules.generics;

import java.util.function.Consumer;

public class Task2 {
	public static void main(String[] args) {
		ArrayIterator<Number> numbers = new ArrayIterator<>(1, 2L, 3f, 4d);
		ArrayIterator<Integer> integers = new ArrayIterator<>(1, 2, 3, 4);
		ArrayIterator<Double> doubles = new ArrayIterator<>(1., 2., 3., 4.);
		
		Consumer<Number> numberConsumer = System.out::println;
		
		numbers.forEach(numberConsumer);
//		integers.forEach(numberConsumer); TODO odkomentuj po zmianie
//		doubles.forEach(numberConsumer); TODO odkomentuj po zmianie
	}
	
	private static final class ArrayIterator<T> {
		private final T[] elements;
		
		@SafeVarargs
		ArrayIterator(T... elements) {
			this.elements = elements;
		}
		
		//TODO Zmodyfikuj sygnaturę metody tak, żeby funkcja była w stanie przyjąć również typy konkretniejsze od typu T.
		void forEach(Consumer<T> consumer) {
			for (T element : elements) {
				consumer.accept(element);
			}
		}
	}
}
