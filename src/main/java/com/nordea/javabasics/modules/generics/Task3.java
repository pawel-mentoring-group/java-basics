package com.nordea.javabasics.modules.generics;

public class Task3 {
	public static void main(String[] args) {
		acceptObject(new ConcreteClass1());
		acceptObject(new ConcreteClass2());
	}
	
	//TODO Zmodyfikuj typ T tak, żeby akceptował obiekty typu BaseClass implementujące interfejs Interface.
	private static <T> void acceptObject(T object) {
//		object.baseMethod(); TODO odkomentuj po zmianie
//		object.interfaceMethod(); TODO odkomentuj po zmianie
	}
	
	private static abstract class BaseClass {
		abstract void baseMethod();
	}
	
	private interface Interface {
		void interfaceMethod();
	}
	
	private static final class ConcreteClass1 extends BaseClass implements Interface {
		@Override
		void baseMethod() {
			System.out.println("ConcreteClass1 - baseMethod");
		}
		
		@Override
		public void interfaceMethod() {
			System.out.println("ConcreteClass1 - interfaceMethod");
		}
	}
	
	private static final class ConcreteClass2 extends BaseClass implements Interface {
		@Override
		void baseMethod() {
			System.out.println("ConcreteClass2 - baseMethod");
		}
		
		@Override
		public void interfaceMethod() {
			System.out.println("ConcreteClass2 - interfaceMethod");
		}
	}
}
