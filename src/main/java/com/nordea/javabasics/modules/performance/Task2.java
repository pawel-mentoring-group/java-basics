package com.nordea.javabasics.modules.performance;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;

/*
 * Zoptymalzuj działanie poniższego kodu.
 */
public class Task2 {
	/**
	 * Kod wypisuje czy podana data jest dniem powszednim czy weekendem.
	 */
	public static void main(String[] args) {
		LocalDate date = LocalDate.of(2000, Month.JANUARY, 1);
		
		if (date.getDayOfWeek() == DayOfWeek.MONDAY ||
			date.getDayOfWeek() == DayOfWeek.TUESDAY ||
			date.getDayOfWeek() == DayOfWeek.WEDNESDAY ||
			date.getDayOfWeek() == DayOfWeek.THURSDAY ||
			date.getDayOfWeek() == DayOfWeek.FRIDAY
		) {
			System.out.println(date + " is a weekday.");
		} else if (date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY) {
			System.out.println(date + " is a weekend.");
		}
	}
}
