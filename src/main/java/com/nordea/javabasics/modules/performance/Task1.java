package com.nordea.javabasics.modules.performance;

/*
 * Zoptymalzuj działanie poniższego kodu.
 *
 * P.S.: Są co najmniej 3 możliwe optymalizacje.
 */
public class Task1 {
	/**
	 * Kod wypisuje 10 kolejnych znaków alfabetu zaczynając od 'a'.
	 */
	public static void main(String[] args) {
		int numberOfCharacters = 10;
		
		String string = "";
		for (char character = 'a'; character < 'a' + numberOfCharacters; character++) {
			string += character;
		}
		
		System.out.println(string);
	}
}
