package com.nordea.javabasics.tasks.atm;

enum Bill {
	$1(1),
	$2(2),
	$5(5),
	$10(10),
	$20(20),
	$50(50),
	$100(100);
	
	private final long faceValue;
	
	Bill(long faceValue) {
		this.faceValue = faceValue;
	}
	
	long getFaceValue() {
		return faceValue;
	}
}