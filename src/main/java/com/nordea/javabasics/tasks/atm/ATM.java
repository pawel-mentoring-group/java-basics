package com.nordea.javabasics.tasks.atm;

import com.google.common.collect.Multiset;

/**
 * Zaimplementuj bankomat, który wypłaca podaną ilość pieniędzy użytkownikowi po podaniu prawidłowego NIKu i PINu.
 *
 * Maszyna powinna wypłacać pieniądze w największych możliwych nominałach.
 *
 * Po implementacji powinien prawidłowo przechodzić test zdefiniowany w klasie ATMTest.
 */
class ATM {
	Multiset<Bill> withdrawMoney(String nik, String pin, long amount) throws InvalidNikOrPinException {
		throw new UnsupportedOperationException("TODO");
	}
}
